package ru.t1consulting.vmironova.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.endpoint.ITaskEndpoint;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    public void renderTasks(@Nullable final List<TaskDTO> tasks) {
        if (tasks == null) return;
        tasks.forEach(m -> {
            showTask(m);
            System.out.println();
        });
    }

    public void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + task.getCreated());
    }

}
