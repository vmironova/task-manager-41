package ru.t1consulting.vmironova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.request.ProjectListRequest;
import ru.t1consulting.vmironova.tm.enumerated.Sort;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Display project list.";

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<ProjectDTO> projects = getProjectEndpoint().listProject(request).getProjects();
        if (projects == null) return;
        @NotNull final int[] index = {1};
        projects.forEach(m -> {
            System.out.println(index[0] + ". " + m);
            index[0]++;
        });
    }

}
