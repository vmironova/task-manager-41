package ru.t1consulting.vmironova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.request.TaskShowByProjectIdRequest;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final List<TaskDTO> tasks = getTaskEndpoint().showByProjectIdTask(request).getTasks();
        renderTasks(tasks);
    }

}
