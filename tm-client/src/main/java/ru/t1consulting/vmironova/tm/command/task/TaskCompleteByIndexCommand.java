package ru.t1consulting.vmironova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.dto.request.TaskCompleteByIndexRequest;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    public static final String NAME = "task-complete-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken());
        request.setIndex(index);
        getTaskEndpoint().completeByIndexTask(request);
    }

}
