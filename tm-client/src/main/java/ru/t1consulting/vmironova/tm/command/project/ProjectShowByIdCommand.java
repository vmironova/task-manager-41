package ru.t1consulting.vmironova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.request.ProjectShowByIdRequest;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);
        @Nullable final ProjectDTO project = getProjectEndpoint().showByIdProject(request).getProject();
        showProject(project);
    }

}
