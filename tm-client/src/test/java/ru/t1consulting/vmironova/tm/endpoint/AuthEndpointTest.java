package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.vmironova.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.dto.request.*;
import ru.t1consulting.vmironova.tm.marker.IntegrationCategory;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.service.PropertyService;

import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.loginUser(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        userEndpointClient.registryUser(request);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    private String getUserToken() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        return authEndpointClient.loginUser(request).getToken();
    }

    @Test
    public void loginUser() {
        @Nullable final String token = getUserToken();
        Assert.assertNotNull(token);
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        Assert.assertNotNull(authEndpointClient.viewProfileUser(viewRequest).getUser());
    }

    @Test
    public void logoutUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        authEndpointClient.logoutUser(request);
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        Assert.assertThrows(Exception.class, () -> authEndpointClient.viewProfileUser(viewRequest));
    }

    @Test
    public void viewProfileUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(token);
        @Nullable final UserDTO user = authEndpointClient.viewProfileUser(request).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST_LOGIN, user.getLogin());
    }

}
