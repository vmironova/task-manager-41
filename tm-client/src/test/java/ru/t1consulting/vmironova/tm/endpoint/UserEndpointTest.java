package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.vmironova.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.dto.request.*;
import ru.t1consulting.vmironova.tm.marker.IntegrationCategory;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.service.PropertyService;

import java.util.Locale;

import static ru.t1consulting.vmironova.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.loginUser(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        userEndpointClient.registryUser(request);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    @Nullable
    private String getUserToken(final String login, final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpointClient.loginUser(request).getToken();
    }

    @Test
    public void changePasswordUser() {
        @Nullable String token = getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(token);
        request.setNewPassword(USER_TEST_PASSWORD.toLowerCase(Locale.ROOT));
        Assert.assertNotNull(userEndpointClient.changePasswordUser(request));
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(token);
        authEndpointClient.logoutUser(logoutRequest);
        token = getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD.toLowerCase(Locale.ROOT));
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordRequest newPasswordRequest = new UserChangePasswordRequest(token);
        newPasswordRequest.setNewPassword(USER_TEST_PASSWORD);
        Assert.assertNotNull(userEndpointClient.changePasswordUser(newPasswordRequest));
    }

    @Test
    public void lockUser() {
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(userEndpointClient.lockUser(request));
        Assert.assertThrows(Exception.class, () -> getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
        unlockUser();
    }

    @Test
    public void registryUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(removeRequest);
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        userEndpointClient.registryUser(request);
        Assert.assertNotNull(getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
    }

    @Test
    public void removeUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(userEndpointClient.removeUser(removeRequest));
        Assert.assertThrows(Exception.class, () -> getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        userEndpointClient.registryUser(request);
    }

    @Test
    public void unlockUser() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(userEndpointClient.unlockUser(request));
        Assert.assertNotNull(getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
    }

    @Test
    public void updateProfileUser() {
        @Nullable final String token = getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setFirstName(USER_TEST_FIRST_NAME);
        request.setLastName(USER_TEST_LAST_NAME);
        request.setMiddleName(USER_TEST_MIDDLE_NAME);
        Assert.assertNotNull(userEndpointClient.updateProfileUser(request));
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        @Nullable final UserDTO user = authEndpointClient.viewProfileUser(viewRequest).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST_FIRST_NAME, user.getFirstName());
        Assert.assertEquals(USER_TEST_LAST_NAME, user.getLastName());
        Assert.assertEquals(USER_TEST_MIDDLE_NAME, user.getMiddleName());
    }

}
