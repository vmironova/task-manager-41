package ru.t1consulting.vmironova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.dto.model.SessionDTO;
import ru.t1consulting.vmironova.tm.enumerated.Role;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static SessionDTO USER_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER_SESSION2 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER_SESSION3 = new SessionDTO();

    @NotNull
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<SessionDTO> USER_SESSION_LIST = Arrays.asList(USER_SESSION1, USER_SESSION2, USER_SESSION3);

    @NotNull
    public final static List<SessionDTO> SESSION_LIST = Arrays.asList(USER_SESSION1, USER_SESSION2);

    static {
        USER_SESSION_LIST.forEach(session -> session.setRole(Role.USUAL));
    }

}
