package ru.t1consulting.vmironova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    void add(@NotNull ProjectDTO project);

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull ProjectDTO project);

    @Update("UPDATE tm_project SET name = #{name}, created = #{created}, description = #{description}, " +
            "user_id = #{userId}, status = #{status} WHERE id = #{id}")
    void update(@NotNull ProjectDTO project);

}
