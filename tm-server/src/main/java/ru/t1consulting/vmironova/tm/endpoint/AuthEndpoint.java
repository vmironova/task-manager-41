package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.vmironova.tm.api.service.IAuthService;
import ru.t1consulting.vmironova.tm.api.service.IServiceLocator;
import ru.t1consulting.vmironova.tm.api.service.IUserService;
import ru.t1consulting.vmironova.tm.dto.model.SessionDTO;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.dto.request.UserLoginRequest;
import ru.t1consulting.vmironova.tm.dto.request.UserLogoutRequest;
import ru.t1consulting.vmironova.tm.dto.request.UserViewProfileRequest;
import ru.t1consulting.vmironova.tm.dto.response.UserLoginResponse;
import ru.t1consulting.vmironova.tm.dto.response.UserLogoutResponse;
import ru.t1consulting.vmironova.tm.dto.response.UserViewProfileResponse;
import ru.t1consulting.vmironova.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1consulting.vmironova.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return this.getServiceLocator().getAuthService();
    }

    @NotNull
    private IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse loginUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        try {
            @NotNull final String token = getAuthService().login(request.getLogin(), request.getPassword());
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logoutUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        try {
            getAuthService().logout(session);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse viewProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable UserDTO user;
        try {
            user = getUserService().findOneById(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserViewProfileResponse(user);
    }

}
