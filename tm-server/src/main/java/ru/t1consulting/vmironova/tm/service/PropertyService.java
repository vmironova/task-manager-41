package ru.t1consulting.vmironova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "123456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "321";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "127.0.0.1";

    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "123";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10000";

    @NotNull
    private static final String DATABASE_USERNAME = "database.username";

    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    private static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        //return "1.31.0";
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        //return "Veronika Mironova";
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        //return "vmironova@t1-consulting.ru";
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DATABASE_USERNAME, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DATABASE_PASSWORD, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DATABASE_URL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DATABASE_DRIVER, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, EMPTY_VALUE);
    }

}
