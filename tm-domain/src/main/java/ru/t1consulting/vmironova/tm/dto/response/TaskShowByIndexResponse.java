package ru.t1consulting.vmironova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
