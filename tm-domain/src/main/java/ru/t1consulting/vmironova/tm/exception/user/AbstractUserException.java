package ru.t1consulting.vmironova.tm.exception.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(@NotNull String message) {
        super(message);
    }

    public AbstractUserException(
            @NotNull String message,
            @NotNull Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractUserException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractUserException(
            @NotNull String message,
            @NotNull Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
