package ru.t1consulting.vmironova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_user")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Column(nullable = false)
    @Nullable
    private String login;

    @Column(name = "password")
    @Nullable
    private String passwordHash;

    @Column
    @NotNull
    private String email = "";

    @Column(name = "first_name")
    @NotNull
    private String firstName = "";

    @Column(name = "last_name")
    @NotNull
    private String lastName = "";

    @Column(name = "middle_name")
    @NotNull
    private String middleName = "";

    @Column
    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role = Role.USUAL;

    @Column
    @NotNull
    private Boolean locked = false;

}
